# seq-ord

`seq-ord` generates the function `(a, b) => 1 | 0 | -1` accepted by `Array#sort()` from complex rules.


## Examples

```js
import seqOrd, { Rules } from 'seq-ord';

let sortedBooks = books.sort(seqOrd(function* (a, b) {
  yield Rules.binary(a.available, b.available);
  yield Rules.text(a.title, b.title);
  yield Rules.date(a.date, b.date);
}));
```

```js
let sortedBooks = books.sort(seqOrd(function* (a, b) {
  yield Rules.binaryExt(
    Rules.sequence(function* () { /* both are unavailable */ }),
    Rules.sequence(function* () { /* both are available */ })
  )(a.available, b.available);
}));
```


## Rules

- `binary` – false values first
- `binaryExt(<cmp func>, <cmp func>)` – allows using different rules for `!a && !b` and `a && b`
- `date` – older dates first
- `numeric` – smaller numbers first
- `text(., ., locales, options)` – uses `localeCompare(locales, options)`
- `textCodepoint` – uses `<` and `>`, lowest codepoint first
- `sequence(<generator>, <this>)`
