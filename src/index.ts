export type CompareFn<T> = (a: T, b: T) => number;


export function binary(a: unknown, b: unknown) {
  if (!a && b) return -1;
  if (a && !b) return 1;
  return 0;
}

export function date(a: Date, b: Date) {
  return numeric(a.getTime(), b.getTime());
}

export function numeric(a: number, b: number) {
  return a - b;
}

export function text(a: string, b: string, locales?: string | string[] | undefined, options?: Intl.CollatorOptions) {
  return a.localeCompare(b, locales, options);
}

export function textCodepoint(a: string, b: string) {
  if (a < b) return -1;
  if (a > b) return 1;
  return 0;
}

export function binaryExt(cmpFF: CompareFn<void>, cmpTT: CompareFn<void>) {
  return (a: unknown, b: unknown) => {
    let value = binary(a, b);

    return value === 0
      ? a && b ? cmpTT() : cmpFF()
      : value;
  };
}

export function sequence<T, S = any>(generator: (this: S, a: T, b: T, rules: typeof Rules) => Generator<number>, that?: S) {
  return (a: T, b: T) => {
    let iterator = generator.call(that as S, a, b, Rules);

    for (let value of iterator) {
      if (value) {
        return value;
      }
    }

    return 0;
  };
}


export default sequence;

export const Rules = {
  binary,
  date,
  numeric,
  text,
  textCodepoint,
  binaryExt,
  sequence
};
